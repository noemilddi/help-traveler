$(document).ready(function() {

    $('#buttonLoginModal').click( function() {
        $('#loginModal').modal('show');
    });

    $('#buttonHostUser').click( function () {
        $('#interchangeAlert').modal('show');
        $('#hostUserModal').modal('hide');
    });

    $('#buttonVisitUser').click( function () {
        $('#interchangeAlert').modal('show');
        $('#visitUserModal').modal('hide');
    });

    $('#buttonPack1').click( function () {
        $('#packAlert').modal('show');
    });

    $('#buttonPack2').click( function () {
        $('#packAlert').modal('show');
    });

    $('#buttonPack3').click( function () {
        $('#packAlert').modal('show');
    });

    $('#buttonPack4').click( function () {
        $('#packAlert').modal('show');
    });

    $('#buttonPack5').click( function () {
        $('#packAlert').modal('show');
    });

    $('#buttonHotel1').click( function () {
        $('#hotelAlert').modal('show');
    });

    $('#buttonHotel2').click( function () {
        $('#hotelAlert').modal('show');
    });

    $('#buttonHotel3').click( function () {
        $('#hotelAlert').modal('show');
    });

    $('#buttonHotel4').click( function () {
        $('#hotelAlert').modal('show');
    });

    $('#buttonHotel5').click( function () {
        $('#hotelAlert').modal('show');
    });

    $('#buttonFlight1').click( function () {
        $('#flightAlert').modal('show');
    });

    $('#buttonFlight2').click( function () {
        $('#flightAlert').modal('show');
    });

    $('#buttonFlight3').click( function () {
        $('#flightAlert').modal('show');
    });

    $('#buttonFlight4').click( function () {
        $('#flightAlert').modal('show');
    });

    $('#buttonFlight5').click( function () {
        $('#flightAlert').modal('show');
    });


    $("form[name='flight']").validate({
        rules: {
            source: 'required',
            destination: 'required',
            departuredate: 'required',
            Returndate: 'required'
        },
        messages: {  
            source: 'Insert a source',
            destination: 'Insert a destination',
            departuredate: 'Insert a departure date',
            Returndate: 'Inserte an Return date'
        },  
        submitHandler: function () {
            window.location.href = "http://192.168.1.171/flights.html"
        } 
    });

    $("form[name='hotel']").validate({
        rules: {
            destination2: 'required',
            departuredate2: 'required',
            Returndate2: 'required',
        },
        messages: {  
            destination2: 'Insert a destination',
            departuredate2: 'Insert a departure date',
            Returndate2: 'Inserte an Return date'
        },  
        submitHandler: function () {
            window.location.href = "http://192.168.1.171/hotels.html"
        } 
    });

    $("form[name='flightHotel']").validate({
        rules: {
            source3: 'required',
            destination3: 'required',
            departuredate3: 'required',
            Returndate3: 'required'
        },
        messages: {  
            source3: 'Insert a source',
            destination3: 'Insert a destination',
            departuredate3: 'Insert a departure date',
            Returndate3: 'Inserte an Return date'
        },  
        submitHandler: function () {
            window.location.href = "http://192.168.1.171/flightsPlusHotel.html"
        } 
    });

    $("form[name='feedbackForm']").validate({
        rules: {
            firstname: 'required',
            lastname: 'required',
            emailid: 'required',
            feedback: 'required'
        },
        messages: {  
            firstname: 'Insert a firstname',
            lastname: 'Insert a lastname',
            emailid: 'Insert an email',
            feedback: 'Insert a feedback'
        },  
        submitHandler: function () {
            $('#feedbackAlert').modal('show')
        } 
    });

    $("form[name='opinionUser']").validate({
        rules: {
            firstname2: 'required',
            lastname2: 'required',
            emailid2: 'required',
            feedback2: 'required'
        },
        messages: {  
            firstname2: 'Insert a firstname',
            lastname2: 'Insert a lastname',
            emailid2: 'Insert an email',
            feedback2: 'Insert a feedback'
        },  
        submitHandler: function () {
            $('#userOpinionAlert').modal('show')
        } 
    });

    $("form[name='visitUser']").validate({
        rules: {
            departuredate5: 'required',
            Returndate5: 'required'
        },
        messages: {  
            departuredate5: 'Insert a departure date',
            Returndate5: 'Insert an Return date'
        },  
        submitHandler: function () {
            $('#visitUserModal').modal('hide');
            $('#interchangeAlert').modal('show');
        } 
    });

    $("form[name='login']").validate({
        rules: {
            username: 'required',
            password: 'required'
        },
        messages: {  
            username: 'Insert an username',
            password: 'Insert a password'
        }
    });

    $('#buttonFlightModal').click( function () {
        $('#flightModal').modal('show');
    });
    $('#buttonHotelModal').click( function () {
        $('#hotelModal').modal('show');
    });
    $('#buttonFlightHotelModal').click( function () {
        $('#flightHotelModal').modal('show');
    });
    $('#buttonHostUserModal').click( function () {
        $('#hostUserModal').modal('show');
    });
    $('#buttonVisitUserModal').click( function () {
        $('#visitUserModal').modal('show');
    });
    
});